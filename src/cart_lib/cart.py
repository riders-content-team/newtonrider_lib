
import rospy
from std_msgs.msg import String,Float32
from geometry_msgs.msg import Pose
import json
from gazebo_msgs.srv import GetModelState
from std_srvs.srv import Trigger, TriggerResponse
import time
import math
import os
import requests
import subprocess
import atexit
from newtonrider_arena.srv import MoveCart
from newtonrider_arena.srv import SpawnModel
from newtonrider_arena.srv import RemoveModel
from gazebo_msgs.srv import SpawnModel as spn

# We don't use a real robot - just a simple class to pass messages
class Cart:
    def __init__(self, robot_name):
        rospy.init_node("cart")
        self.init_services()
        time.sleep(0.5)
        self.bCalledGo = False
        self.pub = rospy.Publisher("/mass",Float32,queue_size=1)
        self.mass_msg = Float32()
    
    def init_services(self):
        try:
            rospy.wait_for_service("/move_cart", 5.0)
            self.move_cart = rospy.ServiceProxy('move_cart', MoveCart)

        except (rospy.ServiceException, rospy.ROSException), e:
            rospy.logerr("Service call failed: %s" % (e,))

        try:
            rospy.wait_for_service("/spawn_model", 5.0)
            self.spawn_model = rospy.ServiceProxy('spawn_model', SpawnModel)

        except (rospy.ServiceException, rospy.ROSException), e:
            rospy.logerr("Service call failed: %s" % (e,))

        try:
            rospy.wait_for_service("/remove_model", 5.0)
            self.remove_model = rospy.ServiceProxy('remove_model', RemoveModel)

        except (rospy.ServiceException, rospy.ROSException), e:
            rospy.logerr("Service call failed: %s" % (e,))
        
        # initialize model spawn service
        try:
            rospy.wait_for_service("/gazebo/spawn_sdf_model", 1.0)

            self.spawn_sdf_model = rospy.ServiceProxy('/gazebo/spawn_sdf_model', spn)

        except (rospy.ServiceException, rospy.ROSException), e:
            rospy.logerr("Service call failed: %s" % (e,))

    def go(self, mode, mass):
        if mass > 10000:
            mass = 10000
        elif mass < 1:
            mass = 1
        force = 1000 # might make this a variable for a later lesson
        if not self.bCalledGo:
            self.mass_msg.data = mass
            self.pub.publish(self.mass_msg)
            max_mass = 10000.0
            max_force = 10000.0
            if force > max_force:
                force = max_force
            if mass > max_mass:
                    mass = max_mass
            self.bCalledGo = True
            self.remove_model("flat_barrier")
            time.sleep(0.5) # hack avoid remove spawn out of order threads
            if mode == 1:
                track_angle = math.pi / 6.0
                # hack position - original model was setup to have required world offset so we're sliding down the ramp
                self.spawn_model("barreer", "flat_barrier", 0, -11.1, 0.37-10.0 * math.sin(track_angle))
            self.move_cart("cartname", mode, mass, force)


